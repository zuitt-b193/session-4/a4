package com.zuitt.batch193;

import java.util.ArrayList;

public class Course {

    //properties
    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private String instructor;
    private ArrayList enrollees;



    //empty constructor
    public Course(){

    };

    //parameterized
    public Course(String name, String description, int seats, double fee, String startDate, String endDate, String instructor){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }

    //getter
    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public int getSeats() {
        return this.seats;
    }

    public double getFee() {
        return this.fee;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public String getInstructor() {
        return this.instructor;
    }
    public ArrayList getEnrollees() {
        return this.enrollees;
    }


    //setter
    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setInstructor(String instructor){
        this.instructor = instructor;
    }




}
