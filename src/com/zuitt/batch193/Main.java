package com.zuitt.batch193;

public class Main {

    public static void main(String[] args){


        //User
        User newUser = new User("Tee Jae", "Calinao", 25, "Antipolo City");
        System.out.println("User's first name:");
        System.out.println(newUser.getFirstName());
        System.out.println("User's last name:");
        System.out.println(newUser.getLastName());
        System.out.println("User's age:");
        System.out.println(newUser.getAge());
        System.out.println("User's address:");
        System.out.println(newUser.getAddress());

        //Course
        Course newCourse = new Course();
        newCourse.setName("Physics 101");
        newCourse.setDescription("Learn Physics");
        newCourse.setSeats(30);
        newCourse.setInstructor("Tee Jae");

        System.out.println("Course's name:");
        System.out.println(newCourse.getName());
        System.out.println("Course's description:");
        System.out.println(newCourse.getDescription());
        System.out.println("Course's seats:");
        System.out.println(newCourse.getSeats());
        System.out.println("Course's instructor's first name:");
        System.out.println(newCourse.getInstructor());




    }
}
